from selenium import webdriver
import unittest

__author__ = 'Dion'


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_retrive_it_later(self):
        # Edith has heard about a cool new online shop.
        # She goes to check out its homepage.
        self.browser.get("http://localhost:8000/rango")

        # She notices the page title and header mention Rango
        self.assertIn('Rango', self.browser.title)
        self.fail('Finish the test!')

        # She is invited to enter a new category straight away
        # ...

if __name__ == '__main__':
    unittest.main(warnings='ignore')