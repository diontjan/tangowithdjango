### TangoWithDjango

Curriculum: [Tango With Django 1.7](http://www.tangowithdjango.com/book17)

IDE: [PyCharm Community Edition 4.5.4](https://www.jetbrains.com/pycharm/)  

VCS: either [Git for Windows 2.6.2](https://git-scm.com/download/win) or [Mercurial for Windows 3.5.2](https://www.mercurial-scm.org/)  

Interpreter: [Python for Windows 3.5.0](https://www.python.org/downloads/windows/)

Framework: [Django 1.8.5](https://www.djangoproject.com/)  

Browser automation: [Selenium 2.48.0](http://selenium-python.readthedocs.org/)  

#### Note:  
* If you use Bitbucket then you should install [this additional PyCharm plugin](https://bitbucket.org/dmitry_cherkas/jetbrains-bitbucket-connector/downloads). 
Remember to enable hg4idea bundled plugin.
