#### Setup Python
1. Install Python for Windows (C:\Python\Python35-32)  
 a. Remember to add Python to PATH
2. Install setuptools for downloading pip
 a. Download [ez_setup.py](https://bootstrap.pypa.io/ez_setup.py)  
 b. Run **python ez_setup.py**
3. Install pip  
 a. Run **python easy_install pip**
4. Install virtualenv  
 a. Run **pip install virtualenv**


#### Create virtual environment  
Reading: [A non-magical introduction to Pip and Virtualenv for Python beginners](http://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python/)  

1. Create working folder for a new app  
 a. Create a folder named _TangoWithDjango_  
  b. Change directory into it
2. Create virtual environment named _env_ inside the working folder.  
 a. Run **virtualenv env**
3. Activate the new virtual environment  
 a. Inside TangoWithDjango, change directory to _env\Scripts_  
 b. Run **activate** -- (in contrast of deactivate)  
4. Install required library for the project  
 a. Run **pip install django**  
 b. Run **pip install pillow**  
5. Generate requirements.txt listing all required library   
 a. Run **pip freeze > requirements.txt**  
 
Note:  
* The _env_ is a convention, and you should not commit this folder into VCS.  
* You can use the _requirements.txt_ to setup same environment in other machines (replacing step #4) by running **pip install -r requirements.txt**


#### Create Django project
1. Change directory to the project working folder, and then activate its virtual environment.
2. Create a Django project  
 a. You are at the root of the working folder  
 b. Run **python env\Scripts\django-admin.py startproject tango_with_django_project**  
 c. Two new items have been created  
 d. Run **python tango_with_django_project\manage.py runserver**  
 e. Open a browser accessing http://127.0.0.1:8000  

Note:  
* Remember to activate virtual environment before running manage.py [migrate, makemigrations]. You can do this in either PyCharm or PowerShell.  


#### Create Django application
1. Change directory to _tango_with_django_project_  
2. Run **python manage.py startapp rango**  
3. You'll see a new _rango_ folder created in tango_with_django_project  


#### Configure PyCharm Community
1. Run PyCharm  
2. Open a project by selecting **TangoWithDjango** folder, not tango_with_django_project  
3. Click Edit Configurations... (a button at upper right side)  
 a. Click the + icon for adding new configuration, then choose Python  
 b. Set Name to TangoWithDjango  
 c. Set Script to TangoWithDjango\tango_with_django_project\manage.py  
 d. Set Script parameters to runserver  
 e. Click OK  
 f. Run TangoWithDjango, then try accessing http://127.0.0.1:8000
